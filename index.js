const compose = ( patternFn = (p) => `:${ p }` ) => ( statics, ... values ) => {

    var generator = ( params = {} ) => {

        var base = [ statics[0] ];
        for( let [ ix, value ] of values.entries() )
            base.push( params[ value ], statics[ ix + 1 ] );
        return base.join("");

    };
    
    var patternParms = {};
    for( let value of values )
        patternParms[ value ] = patternFn( value );

    return Object.assign( generator, { 
        params  : values,
        pattern : generator( patternParms )
    } );

};

module.exports = { compose };
